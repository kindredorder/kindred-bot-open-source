package com.kindredorder.discord.google;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.*;
import com.google.api.services.sheets.v4.Sheets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

public class GoogleSheets {
   /** Application name. */
   private static final String APPLICATION_NAME =
         "Google Sheets API Java GoogleSheets";

   /** Directory to store user credentials for this application. */
   private static final java.io.File DATA_STORE_DIR = new java.io.File(
         System.getProperty("user.home"), ".credentials/sheets.googleapis.com-java-GoogleSheets");

   /** Global instance of the {@link FileDataStoreFactory}. */
   private static FileDataStoreFactory DATA_STORE_FACTORY;

   /** Global instance of the JSON factory. */
   private static final JsonFactory JSON_FACTORY =
         JacksonFactory.getDefaultInstance();

   /** Global instance of the HTTP transport. */
   private static HttpTransport HTTP_TRANSPORT;

   /** Global instance of the scopes required by this GoogleSheets.
    *
    * If modifying these scopes, delete your previously saved credentials
    * at ~/.credentials/sheets.googleapis.com-java-GoogleSheets
    */
   private static final List<String> SCOPES =
         Arrays.asList(SheetsScopes.SPREADSHEETS_READONLY);

   static {
      try {
         HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
         DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
      } catch (Throwable t) {
         t.printStackTrace();
         System.exit(1);
      }
   }

   private static Logger log = LoggerFactory.getLogger(GoogleSheets.class.getName());

   /**
    * Creates an authorized Credential object.
    * @return an authorized Credential object.
    * @throws IOException
    */
   public static Credential authorize() throws IOException {
      // Load client secrets.
      /*InputStream in =
            GoogleSheets.class.getResourceAsStream("/client_secret.json");
      GoogleClientSecrets clientSecrets =
            GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
      */
      // Build flow and trigger user authorization request.
      /*GoogleAuthorizationCodeFlow flow =
            new GoogleAuthorizationCodeFlow.Builder(
                  HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                  .setDataStoreFactory(DATA_STORE_FACTORY)
                  .setAccessType("offline")
                  .build();*/


      Credential credential = null;
      try
      {
         // TODO - Use your google service account id
         credential = new GoogleCredential.Builder()
               .setTransport(HTTP_TRANSPORT)
               .setJsonFactory(JSON_FACTORY)
               .setServiceAccountId("your-google-service-account-id")
               .setServiceAccountPrivateKeyFromP12File(getP12File())
               .setServiceAccountScopes(SCOPES).build();
         credential.refreshToken();
      }
      catch (GeneralSecurityException e)
      {
         log.error(e.getMessage());
      }
      return credential;
   }

   private static File getP12File()
   {
      File file = null;
      String resource = "/client_secret.p12";
      URL res = GoogleSheets.class.getResource(resource);
      if (res.toString().startsWith("jar:")) {
         try {
            InputStream input = GoogleSheets.class.getResourceAsStream(resource);
            file = File.createTempFile("tempfile", ".tmp");
            OutputStream out = new FileOutputStream(file);
            int read;
            byte[] bytes = new byte[1024];

            while ((read = input.read(bytes)) != -1) {
               out.write(bytes, 0, read);
            }
            file.deleteOnExit();
         } catch (IOException ex) {
            log.error(ex.getMessage());
         }
      } else {
         //this will probably work in your IDE, but not from a JAR
         file = new File(res.getFile());
      }

      if (file != null && !file.exists()) {
         throw new RuntimeException("Error: File " + file + " not found!");
      }
      return file;
   }

   /**
    * Build and return an authorized Sheets API client service.
    * @return an authorized Sheets API client service
    * @throws IOException
    */
   public static Sheets getSheetsService() throws IOException {
      Credential credential = authorize();
      return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
            .setApplicationName(APPLICATION_NAME)
            .build();
   }
}
