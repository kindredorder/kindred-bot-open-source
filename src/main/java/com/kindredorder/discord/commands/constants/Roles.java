package com.kindredorder.discord.commands.constants;

/**
 * Created by insai on 5/22/2017.
 */
public final class Roles
{
   public static final String ADMIN = "Admins";
   public static final String OFFICER = "Officers";
   public static final String DPS_LEADER = "DPS Leader";
   public static final String RAIDER = "Raider";
   public static final String SOCIAL = "Social";
   public static final String[] OFFICER_ROLES = {Roles.DPS_LEADER, Roles.OFFICER, Roles.ADMIN};
   public static final String[] MEMBER_ROLES = {Roles.SOCIAL, Roles.RAIDER, Roles.DPS_LEADER, Roles.OFFICER, Roles.ADMIN};
}
