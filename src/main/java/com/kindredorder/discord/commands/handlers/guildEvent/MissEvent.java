package com.kindredorder.discord.commands.handlers.guildEvent;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.kindredorder.discord.commands.constants.Roles;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.commands.utils.MessageUtils;
import com.kindredorder.discord.data.Event;
import com.kindredorder.discord.data.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.util.List;

/**
 * Created by insai on 5/22/2017.
 */
public class MissEvent
{
   private static final String NAME = "Miss an Event";
   private static Logger log = LoggerFactory.getLogger(MissEvent.class.getName());

   private String[] requiredRoles = {Roles.ADMIN, Roles.OFFICER, Roles.DPS_LEADER, Roles.RAIDER, Roles.SOCIAL};

   public MissEvent()
   {
      super();
   }

   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "notattending",
         description = "Indicate that you will not be attending an event." ,
         usage = "!notattending \"<event id>\" \"<reason for missing event>\""
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void missEvent(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         if (CommonUtils.hasRole(event.getMessage().getAuthor(), event.getMessage().getGuild(), requiredRoles))
         {
            final java.util.List<String> parts = CommonUtils.getTokens(event.getMessage().getContent());
            if (validateParts(parts))
            {
               try
               {
                  final Event evt = Event.byId(parts.get(0));
                  if (evt != null)
                  {
                     Member m = Member.byDiscordId(event.getMessage().getAuthor().getID());
                     if (m == null)
                     {
                        m = new Member(event.getMessage().getAuthor().getID(), "US/Central", null);
                        m.store();
                     }
                     evt.addNotAttending(m, parts.get(1));
                     evt.store();
                     MessageUtils.respondInPrivateMessage(event, builder, "You have indicated you will not be attending the event [" + evt.getName() + "]. If you change your mind for whatever reason, reply to me with the command ```!attending \"" +evt.getId() + "\"```");
                     MessageUtils.respondInChannel(event, builder, event.getMessage().getAuthor().mention() + " has indicated that they will not be attending the event [" + evt.getName() + "] for reason [" + parts.get(1) + "].", "officers");
                  }
                  else
                  {
                     MessageUtils.respondInPrivateMessage(event, builder, "There was an error processing your request - No event with ID [" + parts.get(0) + "] was found.");
                  }
               }
               catch (final Exception e)
               {
                  MessageUtils.respond(event, builder, "There was an error while processing your request to indicate you will not attend an Event.");
               }
            }
            else
            {
               MessageUtils.respond(event, builder, "Your request did not match the usage requirements. Please type \"?help !notattendingevent\" for details");
            }
         }
         else
         {
            MessageUtils.respondInPrivateMessage(event, builder, "You do not have permission to use this command.");
         }
         try
         {
            if (!event.getMessage().getChannel().isPrivate())
            {
               event.getMessage().delete();
            }
         }
         catch (final Exception e)
         {
            log.warn("Failed to remove command message. " + e.getMessage());
         }
      });
   }

   private boolean validateParts(final java.util.List<String> parts)
   {
      return parts.size() == 2 && parts.get(0).length() > 0 && parts.get(1).length() > 0;
   }
}
