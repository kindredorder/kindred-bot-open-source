package com.kindredorder.discord.commands.handlers.guildEvent;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.kindredorder.discord.commands.constants.Roles;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.commands.utils.MessageUtils;
import com.kindredorder.discord.commands.utils.ValidatorUtils;
import com.kindredorder.discord.data.Event;
import com.kindredorder.discord.data.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by insai on 5/22/2017.
 */
public class AddEvent
{
   private static final String NAME = "Create Event";
   private static Logger log = LoggerFactory.getLogger(AddEvent.class.getName());

   private String[] requiredRoles = {Roles.ADMIN, Roles.OFFICER, Roles.DPS_LEADER};

   public AddEvent()
   {
      super();
   }

   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "addevent",
         description = "Creates a guild event that members can RSVP to." ,
         usage = "!addevent \"<event name>\" \"<event description>\" \"<start time/date>\" \"<end time/date>\"\n\tStart and End times are in the format \"MM/DD/YY-HH:MM<AM/PM>-<Zone ID>\". A zone id is a string representing the timezone, for example \"US/Central\""
   )
   @Permission(
      allowPrivateMessage = true
   )
   public void addEvent(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         if (CommonUtils.hasRole(event.getMessage().getAuthor(), event.getMessage().getGuild(), requiredRoles))
         {
            final java.util.List<String> parts = CommonUtils.getTokens(event.getMessage().getContent());
            if (validateParts(parts, false))
            {
               try
               {
                  final Event evt = new Event(parts);
                  Member m = Member.byDiscordId(event.getMessage().getAuthor().getID());
                  if (m == null)
                  {
                     m = new Member(event.getMessage().getAuthor().getID(), "US/Central", null);
                     m.store();
                  }
                  evt.addAttending(m);
                  evt.store();
                  MessageUtils.respondInPrivateMessage(event, builder, "The event [" + evt.getName() + "] was created with ID [" + evt.getId() + "].");
               }
               catch (final Exception e)
               {
                  MessageUtils.respond(event, builder, "There was an error while processing your request to create a new Event.");
               }
            }
            else
            {
               MessageUtils.respond(event, builder, "Your request did not match the usage requirements. Please type \"?help !addevent\" for details");
            }
         }
         else
         {
            MessageUtils.respondInPrivateMessage(event, builder, "You do not have permission to use this command.");
         }
         try
         {
            if (!event.getMessage().getChannel().isPrivate())
            {
               event.getMessage().delete();
            }
         }
         catch (final Exception e)
         {
            log.warn("Failed to remove command message. " + e.getMessage());
         }
      });
   }

   static boolean validateParts(final List<String> parts, boolean edit)
   {
      boolean ret = true;
      int size = 4;
      if (edit)
      {
         size = 5;
      }
      if (parts.size() == size)
      {
         if (edit)
         {
            ret = ret && parts.get(0).length() > 0;
            ret = ret && parts.get(1).length() > 0;
            ret = ret && parts.get(2).length() > 0;
            ret = ret && ValidatorUtils.isDateFormat(parts.get(3)) && ValidatorUtils.isDateFormat(parts.get(4));
         }
         else
         {
            ret = ret && parts.get(0).length() > 0;
            ret = ret && parts.get(1).length() > 0;
            ret = ret && ValidatorUtils.isDateFormat(parts.get(2)) && ValidatorUtils.isDateFormat(parts.get(3));
         }
      }
      else
      {
         ret = false;
      }
      return ret;
   }
}
