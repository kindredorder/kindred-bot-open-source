package com.kindredorder.discord.commands.handlers.guildEvent;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.kindredorder.discord.commands.constants.Roles;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.commands.utils.MessageUtils;
import com.kindredorder.discord.data.Event;
import com.kindredorder.discord.data.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.util.List;

/**
 * Created by insai on 5/22/2017.
 */
public class AttendEvent
{
   private static final String NAME = "Attend Event";
   private static Logger log = LoggerFactory.getLogger(AttendEvent.class.getName());

   private String[] requiredRoles = {Roles.ADMIN, Roles.OFFICER, Roles.DPS_LEADER, Roles.RAIDER, Roles.SOCIAL};

   public AttendEvent()
   {
      super();
   }

   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "attending",
         description = "Removes an event from the database." ,
         usage = "!attending \"<event id>\""
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void attendEvent(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         if (CommonUtils.hasRole(event.getMessage().getAuthor(), event.getMessage().getGuild(), requiredRoles))
         {
            final java.util.List<String> parts = CommonUtils.getTokens(event.getMessage().getContent());
            if (validateParts(parts))
            {
               try
               {
                  final Event evt = Event.byId(parts.get(0));
                  if (evt != null)
                  {
                     Member m = Member.byDiscordId(event.getMessage().getAuthor().getID());
                     if (m == null)
                     {
                        m = new Member(event.getMessage().getAuthor().getID(), "US/Central", null);
                        m.store();
                     }
                     evt.addAttending(m);
                     evt.store();
                     MessageUtils.respondInPrivateMessage(event, builder, "You have registered for the event [" + evt.getName() + "].");
                  }
                  else
                  {
                     MessageUtils.respondInPrivateMessage(event, builder, "There was an error processing your request - No event with ID [" + parts.get(0) + "] was found.");
                  }
               }
               catch (final Exception e)
               {
                  MessageUtils.respond(event, builder, "There was an error while processing your request to attend an Event.");
               }
            }
            else
            {
               MessageUtils.respond(event, builder, "Your request did not match the usage requirements. Please type \"?help !attendingevent\" for details");
            }
         }
         else
         {
            MessageUtils.respondInPrivateMessage(event, builder, "You do not have permission to use this command.");
         }
         try
         {
            if (!event.getMessage().getChannel().isPrivate())
            {
               event.getMessage().delete();
            }
         }
         catch (final Exception e)
         {
            log.warn("Failed to remove command message. " + e.getMessage());
         }
      });
   }

   private boolean validateParts(final java.util.List<String> parts)
   {
      return parts.size() == 1 && parts.get(0).length() > 0;
   }
}
