package com.kindredorder.discord.commands.handlers;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.data.Frenzy;
import com.kindredorder.discord.db.Access;
import com.kindredorder.discord.commands.utils.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by insai on 5/17/2017.
 */
public class FrenzyHandler
{
   private static Logger log = LoggerFactory.getLogger(FrenzyHandler.class.getName());

   private static final String NAME = "Frenzy Generator";
   private static final String ADD_NAME = "Add a Frenzy";
   private static final String REMOVE_NAME = "Remove a Fenzy";

   private ConcurrentMap<String, ConcurrentMap<String, Long>> previousExecutionTime;
   private Access db;

   public FrenzyHandler()
   {
      previousExecutionTime = new ConcurrentHashMap<>();
   }

   public FrenzyHandler(final ConcurrentMap<String, ConcurrentMap<String, Long>> lastExecuted, final Access access)
   {
      previousExecutionTime = lastExecuted;
      db = access;
   }

   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "frenzy",
         description = "Responds with a random frenzy"
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void frenzyCommand(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         if (event.getMessage().getChannel().isPrivate() || event.getMessage().getChannel().getName().equals("lore"))
         {
            final Map<String, Long> last_run = CommonUtils.getLastRunMap(event, previousExecutionTime);
            final Long last_request = last_run.get("!frenzy");
            final Date now = new Date();
            if (last_request == null || now.getTime() - last_request > 1800000)
            {
               last_run.put("!frenzy", now.getTime());
               final Frenzy frenzy = Frenzy.getRandomFrenzy();
               if (frenzy != null)
               {
                  frenzy.requester(event.getMessage().getAuthor().mention());
                  MessageUtils.respond(event, builder, frenzy.toString());
               } else
               {
                  MessageUtils.respondInPrivateMessage(event, builder, "There are no frenzy posts registered in my database. Please consider adding one with !addfrenzy <message>.");
               }
            }
            else
            {
               MessageUtils.respondInPrivateMessage(event, builder, "This command is on cooldown for for " + ((1800000 - (now.getTime() - last_request)) / 1000) + " seconds.");
            }
         }
         else
         {
            MessageUtils.respondInPrivateMessage(event, builder, "This command is restricted to the #lore channel, and private messages.");
         }
         try
         {
            if (!event.getMessage().getChannel().isPrivate())
            {
               event.getMessage().delete();
            }
         }
         catch (final Exception e)
         {
            log.warn("Failed to remove command message. " + e.getMessage());
         }
      });
   }

   @MainCommand(
         prefix = "!",
         name = ADD_NAME,
         alias = "addfrenzy",
         description = "Adds a Frenzy message to the database"
   )
   @Permission(
         allowPrivateMessage = true,
         forcePrivateReply = true
   )
   public void addFrenzyCommand(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         final Map<String, Long> last_run = CommonUtils.getLastRunMap(event, previousExecutionTime);
         final Long last_request = last_run.get("!addfrenzy");
         final Date now = new Date();
         if (last_request == null || now.getTime() - last_request > 5000)
         {
            last_run.put("!addfrenzy", now.getTime());
            final Frenzy frenzy = Frenzy.fromEvent(event);
            if (frenzy != null)
            {
               frenzy.store();
               MessageUtils.respondInPrivateMessage(event, builder, "Your new frenzy message was stored in the database.");
            }
            else
            {
               MessageUtils.respondInPrivateMessage(event, builder, "There was an error storing your new frenzy message.");
            }
         }
         else
         {
            MessageUtils.respondInPrivateMessage(event, builder, "This command is on cooldown for for " + ((5000 - (now.getTime() - last_request)) / 1000) + " seconds.");
         }
         try
         {
            event.getMessage().delete();
         }
         catch (final Exception e)
         {
            log.warn("Failed to remove command message. " + e.getMessage());
         }
      });
   }

   @MainCommand(
         prefix = "!",
         name = REMOVE_NAME,
         alias = "delfrenzy",
         description = "Removes a Frenzy message from the database"
   )
   @Permission(
         allowPrivateMessage = true,
         permissions = { Permissions.MANAGE_MESSAGES }
   )
   public void delFrenzyCommand(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         final Map<String, Long> last_run = CommonUtils.getLastRunMap(event, previousExecutionTime);
         final Long last_request = last_run.get("!delfrenzy");
         final Date now = new Date();
         if (last_request == null || now.getTime() - last_request > 5000)
         {
            last_run.put("!delfrenzy", now.getTime());
            final String frenzyId = Frenzy.idFromEvent(event);
            Frenzy frenzyToRemove = Frenzy.one(frenzyId);
            if (frenzyToRemove != null)
            {
               frenzyToRemove.remove();
               MessageUtils.respondInPrivateMessage(event, builder, "The frenzy with id [" + frenzyId + "] was removed from the database.");
            }
            else
            {
               MessageUtils.respondInPrivateMessage(event, builder,"There was an error removing the frenzy from the database.");
            }
         }
      });
   }

}
