package com.kindredorder.discord.commands.handlers;

import com.github.alphahelix00.discordinator.d4j.handler.CommandHandlerD4J;
import com.kindredorder.discord.commands.handlers.guildEvent.*;

/**
 * Created by insai on 5/22/2017.
 */
public class GuildEventHandler
{
   public static void registerEvents(final CommandHandlerD4J handler)
   {
      handler.registerAnnotatedCommands(new AddEvent());
      handler.registerAnnotatedCommands(new EditEvent());
      handler.registerAnnotatedCommands(new RemoveEvent());
      handler.registerAnnotatedCommands(new GetEvents());
      handler.registerAnnotatedCommands(new AttendEvent());
      handler.registerAnnotatedCommands(new MissEvent());
   }
}
