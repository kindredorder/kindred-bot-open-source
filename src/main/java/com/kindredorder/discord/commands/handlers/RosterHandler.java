package com.kindredorder.discord.commands.handlers;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.commands.utils.SpreadsheetMessageUtils;
import com.kindredorder.discord.google.GoogleSheets;
import com.kindredorder.discord.commands.utils.MessageUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by insai on 5/17/2017.
 */
public class RosterHandler
{
   private static Logger log = LoggerFactory.getLogger(RosterHandler.class.getName());

   private static final String NAME = "Roster";
   private static final String SSID = SpreadsheetMessageUtils.getSSID();
   // Spreadsheet Ranges
   private static final String TANK_RANGE = "Roster!C6:G10";
   private static final String HEALER_RANGE = "Roster!C14:G24";
   private static final String RANGED_RANGE = "Roster!I6:K24";
   private static final String MELEE_RANGE = "Roster!M6:O24";
   private static final String COMP_RANGE = "Roster!Q5:R9";
   private static final String ARMOR_RANGE = "Roster!Q12:R15";
   private static final String TIER_RANGE = "Roster!Q18:R20";
   // Response Data Keys
   private static final int[] TANK_KEYS = {0, 4};
   private static final int[] HEALER_KEYS = {0, 4};
   private static final int[] RANGED_KEYS = {0, 2};
   private static final int[] MELEE_KEYS = {0, 2};
   private static final int[] COMP_KEYS = {0, 1};
   private static final int[] ARMOR_KEYS = {0, 1};
   private static final int[] TIER_KEYS = {0, 1};

   private ConcurrentMap<String, ConcurrentMap<String, Long>> previousExecutionTime;

   public RosterHandler()
   {
      previousExecutionTime = new ConcurrentHashMap<>();
   }

   public RosterHandler(final ConcurrentMap<String, ConcurrentMap<String, Long>> lastExecuted)
   {
      previousExecutionTime = lastExecuted;
   }

   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "roster",
         description = "Responds with current raid roster information"
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void rosterCommand(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         try {
            final Map<String, Long> last_run = CommonUtils.getLastRunMap(event, previousExecutionTime);
            final Long last_request = last_run.get("!roster");
            final Date now = new Date();
            if (last_request == null || now.getTime() - last_request > 300000)
            {
               last_run.put("!roster", now.getTime());
               final StringBuilder stringBuilder = new StringBuilder();

               stringBuilder.append("Raid Roster\n").append("```");
               Sheets sheetService = GoogleSheets.getSheetsService();

               ValueRange response = sheetService.spreadsheets().values().get(SSID, TANK_RANGE).execute();
               List<List<Object>> results = response.getValues();
               if (results != null || results.size() != 0)
               {
                  buildRosterComponent(results, stringBuilder, "Tanks:", null, TANK_KEYS, "\t", false);
               }

               stringBuilder.append("\n");
               response = sheetService.spreadsheets().values().get(SSID, HEALER_RANGE).execute();
               results = response.getValues();
               if (results != null && results.size() != 0)
               {
                  buildRosterComponent(results, stringBuilder, "Healers:", null, HEALER_KEYS, "\t", false);
               }

               stringBuilder.append("\n");
               response = sheetService.spreadsheets().values().get(SSID, RANGED_RANGE).execute();
               results = response.getValues();
               if (results != null && results.size() != 0)
               {
                  buildRosterComponent(results, stringBuilder, "Ranged DPS:", null, RANGED_KEYS, "\t", false);
               }

               stringBuilder.append("\n");
               response = sheetService.spreadsheets().values().get(SSID, MELEE_RANGE).execute();
               results = response.getValues();
               if (results != null && results.size() != 0)
               {
                  buildRosterComponent(results, stringBuilder, "Melee DPS:", null, MELEE_KEYS, "\t", false);
               }
               stringBuilder.append("```");

               stringBuilder.append("\n\n");
               response = sheetService.spreadsheets().values().get(SSID, COMP_RANGE).execute();
               results = response.getValues();
               if (results != null && results.size() != 0)
               {
                  buildRosterComponent(results, stringBuilder, "Raid Composition:", null, COMP_KEYS, "", true);
               }

               stringBuilder.append("\n\n");
               response = sheetService.spreadsheets().values().get(SSID, ARMOR_RANGE).execute();
               results = response.getValues();
               if (results != null && results.size() != 0)
               {
                  buildRosterComponent(results, stringBuilder, "Armor Type Spread:", null, ARMOR_KEYS, "", true);
               }

               stringBuilder.append("\n\n");
               response = sheetService.spreadsheets().values().get(SSID, TIER_RANGE).execute();
               results = response.getValues();
               if (results != null && results.size() != 0)
               {
                  buildRosterComponent(results, stringBuilder, "Tier Token Spread:", null, TIER_KEYS, "", true);
               }

               MessageUtils.respond(event, builder, stringBuilder.toString());
            }
         }
         catch (final Exception e)
         {
            log.error(e.getMessage());
         }
      });
   }

   private void buildRosterComponent(final List<List<Object>> results, final StringBuilder sb, final String title, final String[] columns, final int[] keys, final String rowStart, final boolean includeCodeHeadings)
   {
      sb.append(title + "\n");
      if (includeCodeHeadings)
      {
         sb.append("```");
      }
      if (columns != null)
      {
         sb.append(StringUtils.rightPad(columns[0], 14, " ")).append(StringUtils.rightPad(columns[1], 24, " ")).append("\n");
      }
      for (List row : results)
      {
         try
         {
            final String col1 = (String) row.get(keys[0]);
            final String col2 = (String) row.get(keys[1]);
            if (col1 != null && !col1.isEmpty())
            {
               sb.append(rowStart).append(StringUtils.rightPad(col1, 14, " ")).append(StringUtils.rightPad(col2, 28, " ")).append("\n");
            }
         }
         catch (final Exception e)
         {
            // Do nothing
            log.warn(e.getMessage());
         }
      }
      if (includeCodeHeadings)
      {
         sb.append("```");
      }
   }
}
