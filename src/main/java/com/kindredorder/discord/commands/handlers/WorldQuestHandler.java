package com.kindredorder.discord.commands.handlers;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.commands.utils.SpreadsheetMessageUtils;
import com.kindredorder.discord.google.GoogleSheets;
import com.kindredorder.discord.commands.utils.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by insai on 5/17/2017.
 */
public class WorldQuestHandler
{
   private static Logger log = LoggerFactory.getLogger(WorldQuestHandler.class.getName());

   private static final String NAME = "World Quests";
   private static final String SSID = SpreadsheetMessageUtils.getSSID();

   // Spreadsheet Ranges
   private static final String WQ_RANGE = "Summary!P9:S";

   // Tabular Column Names
   private static final String[] WQ_COLUMNS = {"Rank", "Name", "World Quests Completed (This Week)"};

   // Response Data Keys
   private static final int[] WQ_KEYS = {0, 1, 2};

   private ConcurrentMap<String, ConcurrentMap<String, Long>> previousExecutionTime;

   public WorldQuestHandler()
   {
      previousExecutionTime = new ConcurrentHashMap<>();
   }

   public WorldQuestHandler(final ConcurrentMap<String, ConcurrentMap<String, Long>> lastExecuted)
   {
      previousExecutionTime = lastExecuted;
   }

   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "wqs",
         description = "Responds with current raid roster world quest rankings"
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void wqsCommand(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         try {
            final Map<String, Long> last_run = CommonUtils.getLastRunMap(event, previousExecutionTime);
            final Long last_request = last_run.get("!wqs");
            final Date now = new Date();
            if (last_request == null || now.getTime() - last_request > 300000)
            {
               last_run.put("!wqs", now.getTime());
               Sheets sheetService = GoogleSheets.getSheetsService();
               ValueRange response = sheetService.spreadsheets().values().get(SSID, WQ_RANGE).execute();
               List<List<Object>> values = response.getValues();
               if (values == null || values.size() == 0)
               {
                  log.warn("Failed to get data from the Google Spreadsheet API.");
                  MessageUtils.respond(event, builder, "Failed to aquire data from remote API.");
               }
               else
               {
                  MessageUtils.respond(event, builder, SpreadsheetMessageUtils.buildMessage(values, "World Quest Rankings:", WQ_COLUMNS, WQ_KEYS));
               }
            }
         }
         catch (final Exception e)
         {
            log.error(e.getMessage());
         }
      });
   }
}
