package com.kindredorder.discord.commands.handlers;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.commands.utils.MessageUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by insai on 5/17/2017.
 */
public class CommandsHandler
{
   private static Logger log = LoggerFactory.getLogger(CommandsHandler.class.getName());

   private static final String NAME = "Available Commands";

   private ConcurrentMap<String, ConcurrentMap<String, Long>> previousExecutionTime;

   public CommandsHandler()
   {
      previousExecutionTime = new ConcurrentHashMap<>();
   }

   public CommandsHandler(final ConcurrentMap<String, ConcurrentMap<String, Long>> lastExecuted)
   {
      previousExecutionTime = lastExecuted;
   }

   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "commands",
         description = "Responds with available commands"
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void commandsCommand(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         final Map<String, Long> last_run = CommonUtils.getLastRunMap(event, previousExecutionTime);
         final Long last_request = last_run.get("!commands");
         final Date now = new Date();
         if (last_request == null || now.getTime() - last_request > 300000)
         {
            last_run.put("!commands", now.getTime());
            final StringBuilder sb = new StringBuilder();
            sb.append("Bot Commands:\n\n");
            sb.append("```");
            // sb.append("If you are not an officer, the output of the command will be sent to you in a private message.");
            sb.append(StringUtils.rightPad("!roster", 10, " ")).append("- Display the raid roster and other raid related information.\n");
            sb.append(StringUtils.rightPad("!ap", 10, " ")).append("- Display rankings of raiders by artifact power obtained this week.\n");
            sb.append(StringUtils.rightPad("!traits", 10, " ")).append("- Display rankings of raiders by artifact traits obtained.\n");
            sb.append(StringUtils.rightPad("!ilvl", 10, " ")).append("- Display rankings of raiders by currently equipped ilvl.\n");
            sb.append(StringUtils.rightPad("!mythics", 10, " ")).append("- Display rankings of raiders by number of mythic dungeons completed this week.\n");
            sb.append(StringUtils.rightPad("!wqs", 10, " ")).append("- Display rankings of raiders by number of world quests completed this week.\n");
            sb.append(StringUtils.rightPad("!frenzy", 10, " ")).append("- Display a random frenzy message submitted by a user.\n");
            sb.append(StringUtils.rightPad("!addfrenzy", 10, " ")).append("- Add a frenzy message. Command format: !addfrenzy <message to add>.\n");
            sb.append("```");
            sb.append("\nTo prevent spamming, each command is limited to run once every 5 minutes.\n");
            sb.append("NOTE: Some data may be inaccurate if your character was added to tracking in the last week.");
            MessageUtils.respond(event, builder, sb.toString());
         }
      });
   }
}
