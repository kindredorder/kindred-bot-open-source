package com.kindredorder.discord.commands.handlers;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.commands.utils.SpreadsheetMessageUtils;
import com.kindredorder.discord.google.GoogleSheets;
import com.kindredorder.discord.commands.utils.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by insai on 5/17/2017.
 */
public class TraitsHandler
{
   private static Logger log = LoggerFactory.getLogger(TraitsHandler.class.getName());

   private static final String NAME = "Weapon Traits";
   private static final String SSID = SpreadsheetMessageUtils.getSSID();

   // Spreadsheet Ranges
   private static final String TRAITS_RANGE = "Summary!H9:K";

   // Tabular Column Names
   private static final String[] TRAIT_COLUMNS = {"Rank", "Name", "Traits"};

   // Response Data Keys
   private static final int[] TRAIT_KEYS = {0, 1, 2};

   private ConcurrentMap<String, ConcurrentMap<String, Long>> previousExecutionTime;

   public TraitsHandler()
   {
      previousExecutionTime = new ConcurrentHashMap<>();
   }

   public TraitsHandler(final ConcurrentMap<String, ConcurrentMap<String, Long>> lastExecuted)
   {
      previousExecutionTime = lastExecuted;
   }

   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "traits",
         description = "Responds with current raid roster artifact weapon trait rankings"
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void traitCommand(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         try {
            final Map<String, Long> last_run = CommonUtils.getLastRunMap(event, previousExecutionTime);
            final Long last_request = last_run.get("!traits");
            final Date now = new Date();
            if (last_request == null || now.getTime() - last_request > 300000)
            {
               last_run.put("!traits", now.getTime());
               Sheets sheetService = GoogleSheets.getSheetsService();
               ValueRange response = sheetService.spreadsheets().values().get(SSID, TRAITS_RANGE).execute();
               List<List<Object>> values = response.getValues();
               if (values == null || values.size() == 0)
               {
                  log.warn("Failed to get data from the Google Spreadsheet API.");
                  MessageUtils.respond(event, builder, "Failed to aquire data from remote API.");
               }
               else
               {
                  MessageUtils.respond(event, builder, SpreadsheetMessageUtils.buildMessage(values, "Artifact Trait Rankings:", TRAIT_COLUMNS, TRAIT_KEYS));
               }
            }
         }
         catch (final Exception e)
         {
            log.error(e.getMessage());
         }
      });
   }
}
