package com.kindredorder.discord.commands.handlers.guildEvent;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.kindredorder.discord.commands.constants.Roles;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.commands.utils.MessageUtils;
import com.kindredorder.discord.data.Event;
import com.kindredorder.discord.data.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by insai on 5/22/2017.
 */
public class EditEvent
{
   private static final String NAME = "Edit Event";
   private static Logger log = LoggerFactory.getLogger(EditEvent.class.getName());

   private String[] requiredRoles = {Roles.ADMIN, Roles.OFFICER, Roles.DPS_LEADER};

   public EditEvent()
   {
      super();
   }

   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "editevent",
         description = "Creates a guild event that members can RSVP to." ,
         usage = "!addevent \"<event id>\" \"<event name>\" \"<event description>\" \"<start time/date>\" \"<end time/date>\"\n\tStart and End times are in the format \"MM/DD/YY-HH:MM<AM/PM>-<Zone ID>\". A zone id is a string representing the timezone, for example \"US/Central\""
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void editEvent(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() -> {
         if (CommonUtils.hasRole(event.getMessage().getAuthor(), event.getMessage().getGuild(), requiredRoles))
         {
            final java.util.List<String> parts = CommonUtils.getTokens(event.getMessage().getContent());
            if (AddEvent.validateParts(parts, true))
            {
               try
               {
                  final String evtId = parts.get(0);
                  parts.remove(0);
                  final Event evt = Event.byId(evtId);
                  evt.fromParts(parts);
                  Member m = Member.byDiscordId(event.getMessage().getAuthor().getID());
                  if (m == null)
                  {
                     m = new Member(event.getMessage().getAuthor().getID(), "US/Central", null);
                     m.store();
                  }
                  evt.addAttending(m);
                  evt.store();
                  MessageUtils.respondInPrivateMessage(event, builder, "Your event was updated.");
               }
               catch (final Exception e)
               {
                  MessageUtils.respond(event, builder, "There was an error while processing your request to create a new Event.");
               }
            }
            else
            {
               MessageUtils.respond(event, builder, "Your request did not match the usage requirements. Please type \"?help !editevent\" for details");
            }
         }
         else
         {
            MessageUtils.respondInPrivateMessage(event, builder, "You do not have permission to use this command.");
         }
         try
         {
            if (!event.getMessage().getChannel().isPrivate())
            {
               event.getMessage().delete();
            }
         }
         catch (final Exception e)
         {
            log.warn("Failed to remove command message. " + e.getMessage());
         }
      });
   }
}
