package com.kindredorder.discord.commands.handlers.guildEvent;

import com.github.alphahelix00.discordinator.d4j.permissions.Permission;
import com.github.alphahelix00.ordinator.commands.MainCommand;
import com.kindredorder.discord.commands.constants.Roles;
import com.kindredorder.discord.commands.utils.CommonUtils;
import com.kindredorder.discord.commands.utils.MessageUtils;
import com.kindredorder.discord.data.Event;
import com.kindredorder.discord.data.Member;
import com.kindredorder.discord.data.Missing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by insai on 5/22/2017.
 */
public class GetEvents
{
   private static final String NAME = "Get Events";
   private static final String EVENT_DETAILS_NAME = "Get Event Details";
   private static Logger log = LoggerFactory.getLogger(GetEvents.class.getName());

   private String[] requiredRoles = {Roles.ADMIN, Roles.OFFICER, Roles.DPS_LEADER, Roles.RAIDER, Roles.SOCIAL};
   @MainCommand(
         prefix = "!",
         name = NAME,
         alias = "getevents",
         description = "Responds with a list of upcoming events.",
         usage = "!getevents \"<limit (optional - default 4. Excessive limit could result in no response due to message size limits)>\""
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void upcomingEvents(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() ->
      {
         if (CommonUtils.hasRole(event.getMessage().getAuthor(), event.getMessage().getGuild(), requiredRoles))
         {
            final java.util.List<String> parts = CommonUtils.getTokens(event.getMessage().getContent());
            try
            {
               final java.util.List<Event> evts = Event.futureEvents();
               if (evts != null)
               {
                  long limit = 4;
                  if (parts.size() > 1)
                  {
                     limit = Integer.parseInt(parts.get(1));
                  }
                  final StringBuilder sb = new StringBuilder();
                  sb.append("**Upcoming events:**\n\n");
                  //DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy hh:mm a VV");
                  if (evts.size() > 0)
                  {
                     for (int i = 0; i < Math.min(evts.size(), limit); i++)
                     {
                        final Event evt = evts.get(i);
                        if (evt != null)
                        {
                           sb.append("\t").append(evt.getName()).append(" - Use the following command for more details: ```!getevent \"").append(evt.getId()).append("\"```\n\n");
                        }
                     }
                     MessageUtils.respondInPrivateMessage(event, builder, sb.toString());
                  } else
                  {
                     MessageUtils.respondInPrivateMessage(event, builder, "There are currently no events scheduled.");
                  }
               }
               else
               {
                  MessageUtils.respondInPrivateMessage(event, builder, "There was an error processing your request - No events were found.");
               }
            }
            catch (final Exception e)
            {
               MessageUtils.respond(event, builder, "There was an error while processing your request for upcoming events.");
            }
         }
         else
         {
            MessageUtils.respondInPrivateMessage(event, builder, "You do not have permission to use this command.");
         }
         try
         {
            if (!event.getMessage().getChannel().isPrivate())
            {
               event.getMessage().delete();
            }
         }
         catch (final Exception e)
         {
            log.warn("Failed to remove command message. " + e.getMessage());
         }
      });
   }

   @MainCommand(
         prefix = "!",
         name = EVENT_DETAILS_NAME,
         alias = "getevent",
         description = "Responds with the details for an event. Requests in #Officers will post in channel rather than in a private message.",
         usage = "!getevents \"<event id>\""
   )
   @Permission(
         allowPrivateMessage = true
   )
   public void getEvent(List<String> args, MessageReceivedEvent event, MessageBuilder builder)
   {
      RequestBuffer.request(() ->
      {
         if (CommonUtils.hasRole(event.getMessage().getAuthor(), event.getMessage().getGuild(), Roles.MEMBER_ROLES))
         {
            final java.util.List<String> parts = CommonUtils.getTokens(event.getMessage().getContent());
            if (validateParts(parts))
            {
               try
               {
                  final Event evt = Event.byId(parts.get(0));
                  if (evt != null)
                  {
                     final StringBuilder sb = new StringBuilder();
                     DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy hh:mm a VV");
                     ZonedDateTime start = ZonedDateTime.ofInstant(Instant.ofEpochMilli(evt.getStartTime()), ZoneId.of(CommonUtils.getMemberForUser(event.getMessage().getAuthor()).getTimezone()));
                     ZonedDateTime end = ZonedDateTime.ofInstant(Instant.ofEpochMilli(evt.getEndTime()), ZoneId.of(CommonUtils.getMemberForUser(event.getMessage().getAuthor()).getTimezone()));
                     sb.append("*").append(evt.getName()).append("*").append("\n")
                           .append("\tEvent Start: ").append(format.format(start)).append("\n")
                           .append("\tEvent End: ").append(format.format(end)).append("\n")
                           .append("\tEvent Description: ").append(evt.getDescription()).append("\n")
                           .append("\tEvent ID: ").append(evt.getId()).append("\n");
                     if (CommonUtils.hasRole(event.getMessage().getAuthor(), event.getMessage().getGuild(), Roles.OFFICER_ROLES))
                     {
                        if (evt.getNotAttending() != null && evt.getNotAttending().size() > 0)
                        {
                           sb.append("\n\t\tNot Attending:").append("\n");
                           for (Missing miss : evt.getNotAttending())
                           {
                              sb.append("\t\t\t").append(event.getClient().getUserByID(miss.getMember().getDiscordId()).mention()).append(" - Reason: ").append(miss.getReason()).append("\n");
                           }
                        }
                     }
                     if (event.getMessage().getChannel().getName().equals("officers"))
                     {
                        MessageUtils.respond(event, builder, sb.toString());
                     }
                     else
                     {
                        sb.append("If you are unable to make it to this event, please send me (the bot) a private message with the following command:\n```!notattending \"").append(evt.getId()).append("\" \"The reason you are not attending\"```\n");
                        MessageUtils.respondInPrivateMessage(event, builder, sb.toString());
                     }
                  }
                  else
                  {
                     MessageUtils.respondInPrivateMessage(event, builder, "There was an error processing your request - No event was found.");
                  }
               }
               catch (final Exception e)
               {
                  MessageUtils.respond(event, builder, "There was an error while processing your request for event details.");
               }
            }
            else
            {
               MessageUtils.respond(event, builder, "Your request did not match the usage requirements. Please type \"?help !getevent\" for details");
            }
         }
         else
         {
            MessageUtils.respondInPrivateMessage(event, builder, "You do not have permission to use this command.");
         }
         try
         {
            if (!event.getMessage().getChannel().isPrivate())
            {
               event.getMessage().delete();
            }
         }
         catch (final Exception e)
         {
            log.warn("Failed to remove command message. " + e.getMessage());
         }
      });
   }

   private boolean validateParts(final java.util.List<String> parts)
   {
      return parts.size() == 1 && parts.get(0).length() > 0;
   }
}
