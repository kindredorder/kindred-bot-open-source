package com.kindredorder.discord.commands.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * Created by insai on 5/21/2017.
 */
public final class MessageUtils
{
   private static Logger log = LoggerFactory.getLogger(MessageUtils.class.getName());

   public static void respondInChannel(MessageReceivedEvent event, MessageBuilder builder, final String message, final String channelName)
   {
      final java.util.List<IChannel> channels = event.getClient().getChannels();
      IChannel channel = null;
      if (channels != null && channels.size() != 0)
      {
         for (final IChannel c : channels)
         {
            if (c.getName().equals(channelName))
            {
               channel = c;
               break;
            }
         }
      }
      sendMessage(builder, message, channel);
   }

   public static void respondInPrivateMessage(MessageReceivedEvent event, MessageBuilder builder, final String message)
   {
      try
      {
         IChannel channel = event.getMessage().getAuthor().getOrCreatePMChannel();
         sendMessage(builder, message, channel);
      } catch (RateLimitException e)
      {
         e.printStackTrace();
      } catch (DiscordException e)
      {
         e.printStackTrace();
      }
   }

   private static void sendMessage(MessageBuilder builder, String message, IChannel channel)
   {
      if (channel != null)
      {
         try
         {
            builder.withChannel(channel).withContent(message).build();
         }
         catch (final RateLimitException e)
         {
            log.error(e.getMessage());
         }
         catch (final DiscordException e)
         {
            log.error(e.getErrorMessage());
         }
         catch (final MissingPermissionsException e)
         {
            log.error(e.getErrorMessage());
         }
      }
      else
      {
         log.error("Failed to respond - Channel does not exist!");
      }
   }

   public static void respond(MessageReceivedEvent event, MessageBuilder builder, final String message)
   {
      sendMessage(builder, message, event.getMessage().getChannel());
   }
}
