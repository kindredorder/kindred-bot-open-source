package com.kindredorder.discord.commands.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by insai on 5/22/2017.
 */
public final class ValidatorUtils
{
   public static boolean isDateFormat(final String input)
   {
      Pattern p = Pattern.compile("\\d\\d\\/\\d\\d\\/\\d\\d-\\d\\d:\\d\\d[A|P]M-[a-zA-Z]+\\/[a-zA-Z]+");
      Matcher m = p.matcher(input);
      return m.matches();
   }

   public static void main(String[] args)
   {
      assert isDateFormat("07/10/17-10:00PM-EST");
      assert !isDateFormat("07/10/17 10:00PM-EST");
   }
}
