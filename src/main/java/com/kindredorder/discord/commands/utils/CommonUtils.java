package com.kindredorder.discord.commands.utils;

import com.kindredorder.discord.data.Member;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by insai on 5/17/2017.
 */
public class CommonUtils
{
   public static ConcurrentMap<String, Long> getLastRunMap(final MessageReceivedEvent event, final ConcurrentMap<String, ConcurrentMap<String, Long>> previousExecutionTime)
   {
      final String channelId = event.getMessage().getChannel().getID();
      ConcurrentMap<String, Long> last_run = previousExecutionTime.computeIfAbsent(channelId, k -> new ConcurrentHashMap<>());
      return last_run;
   }

   public static boolean hasRole(final IUser user, final IGuild guild, final String[] roles)
   {
      boolean ret = false;
      if (roles != null)
      {
         for (int i = 0; i < roles.length; i++)
         {
            if (roles[i] != null)
            {
               ret = hasRole(user, guild, roles[i]);
               if (ret)
               {
                  break;
               }
            }
         }
      }
      return ret;
   }

   public static boolean hasRole(final IUser user, IGuild guild, final String role)
   {
      boolean ret = false;
      if (guild == null)
      {
         // TODO - replace with the ID of your discord server
         guild = user.getClient().getGuildByID("discord server id");
      }
      if (guild != null)
      {
         final java.util.List<IRole> roles = user.getRolesForGuild(guild);
         for (final IRole r : roles)
         {
            if (r.getName().equals(role))
            {
               ret = true;
               break;
            }
         }
      }
      return ret;
   }

   public static java.util.List<String> getTokens(final String command)
   {
      final java.util.List<String> ret = new java.util.ArrayList<>();
      Pattern p = Pattern.compile("\\\"(([^\\\\\\\\\\\"]|\\\\\\\\\\\"|\\\\\\\\(?!\\\"))*)\\\"");
      Matcher m = p.matcher(command);
      while(m.find())
      {
         String token = m.group(1);
         ret.add(token);
      }
      return ret;
   }

   public static Member getMemberForUser(final IUser user)
   {
      Member m = null;
      if (user != null)
      {
         m = Member.byDiscordId(user.getID());
         if (m == null)
         {
              m = new Member(user.getID(), "US/Central", null);
              m.store();
         }
      }
      return m;
   }
}
