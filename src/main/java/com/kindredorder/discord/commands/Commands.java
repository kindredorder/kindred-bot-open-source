package com.kindredorder.discord.commands;

import com.github.alphahelix00.discordinator.d4j.handler.CommandHandlerD4J;
import com.github.alphahelix00.ordinator.Ordinator;
import com.kindredorder.discord.commands.handlers.*;
import com.kindredorder.discord.db.Access;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by insai on 5/17/2017.
 */
public class Commands
{
   private static final ConcurrentMap<String, ConcurrentMap<String, Long>> lastExecuted = new ConcurrentHashMap<>();
   private static final CommandHandlerD4J handler = (CommandHandlerD4J) Ordinator.getCommandRegistry().getCommandHandler();

   public static void registerCommands(final Access db)
   {
      handler.registerAnnotatedCommands(new ArtifactPowerHandler(lastExecuted));
      handler.registerAnnotatedCommands(new CommandsHandler(lastExecuted));
      handler.registerAnnotatedCommands(new FrenzyHandler(lastExecuted, db));
      handler.registerAnnotatedCommands(new IlvlHandler(lastExecuted));
      handler.registerAnnotatedCommands(new MythicsHandler(lastExecuted));
      handler.registerAnnotatedCommands(new RosterHandler(lastExecuted));
      handler.registerAnnotatedCommands(new TraitsHandler(lastExecuted));
      handler.registerAnnotatedCommands(new WorldQuestHandler(lastExecuted));
      GuildEventHandler.registerEvents(handler);
   }
}
