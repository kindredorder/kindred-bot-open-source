package com.kindredorder.discord.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Access
{
   private static final Logger trace = LoggerFactory.getLogger(Access.class.getName());
   private static Access instance = null;

   private org.apache.derby.drda.NetworkServerControl server;
   private javax.persistence.EntityManagerFactory entityManagerFactory;

   private String host;
   private Integer port;
   private String user;
   private String password;
   private String database;
   private String persistenceUnitName;

   private Access(final String theHost,
                  final Integer thePort,
                  final String theUser,
                  final String thePassword,
                  final String theDataBase,
                  final String thePersistenceUnitName)
   {
      super();
      host = theHost;
      port = thePort;
      user = theUser;
      password = thePassword;
      database = theDataBase;
      persistenceUnitName = thePersistenceUnitName;
   }
   public static Access instance()
   {
      return instance;
   }
   public static Access instance(final String theHost,
                                 final Integer thePort,
                                 final String theUser,
                                 final String thePassword,
                                 final String theDataBase,
                                 final String thePersistenceUnitName)
   {
      if (instance == null)
      {
         instance = new Access(theHost, thePort, theUser, thePassword, theDataBase, thePersistenceUnitName);
      }
      return instance;
   }
   public void startServer()
   {
      if (server == null)
      {
         try
         {
            server = new org.apache.derby.drda.NetworkServerControl(java.net.InetAddress.getByName(host), port.intValue());
            server.start(null);
         }
         catch (final Exception e)
         {
            trace.error("{}",e);
            server = null;
         }
         final javax.persistence.EntityManager entityManager = entityManager(); // ensure connectivity
         closeEntityManager(entityManager);
         Runtime.getRuntime().addShutdownHook(new Thread(new Runnable()
         {
            @Override public void run()
            {
               shutdown();
            }
         }));
      }
   }
   private void shutdown()
   {
      synchronized (Access.class)
      {
         if ( entityManagerFactory != null )
         {
            if ( entityManagerFactory.isOpen() )
            {
               entityManagerFactory.close();
            }
            entityManagerFactory = null;
         }
      }
      try
      {
         server.shutdown();
         server = null;
      }
      catch (final Exception e)
      {
         trace.error("issue shutting down server: {}",e);
      }
   }
   public static void closeEntityManager(final javax.persistence.EntityManager entityManager)
   {
      if ( entityManager != null )
      {
         if ( entityManager.isOpen() )
         {
            entityManager.close();
         }
      }
   }
   public javax.persistence.EntityManager entityManager()
   {
      final javax.persistence.EntityManager entityManager;

      final java.util.Properties properties = new java.util.Properties();
      properties.put("eclipselink.logging.level","SEVERE");
      properties.put("javax.persistence.jdbc.user", user);
      properties.put("javax.persistence.jdbc.password", password);

      properties.put("eclipselink.ddl-generation","create-or-extend-tables");

      properties.put("eclipselink.ddl-generation.output-mode","both");

      properties.put("eclipselink.logging.file","/var/log/kindred/derby.log");

      properties.put("javax.persistence.jdbc.driver","org.apache.derby.jdbc.EmbeddedDriver");
      properties.put("javax.persistence.jdbc.url","jdbc:derby:" + database + ";create=true");

      entityManagerFactory = javax.persistence.Persistence.createEntityManagerFactory(persistenceUnitName, properties);
      entityManager = entityManagerFactory.createEntityManager();

      return entityManager;
   }
}
