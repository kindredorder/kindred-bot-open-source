package com.kindredorder.discord.data;

import javax.persistence.*;

/**
 * Created by insai on 5/21/2017.
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "MISSING_EVENT")
@org.eclipse.persistence.annotations.UuidGenerator(name = "uuidGenerator")
public class Missing
{
   @Id
   @GeneratedValue(generator = "uuidGenerator")
   @Column
   private String id;
   @Column
   private String reason;
   @ManyToOne
   @JoinColumn(name="event_id", nullable = false)
   private Event event;
   @ManyToOne
   @JoinColumn(name="member_id", nullable = false)
   private Member member;

   private Missing()
   {
      super();
   }

   public Missing(final Event e, final Member m, final String r)
   {
      this();
      event = e;
      member = m;
      reason = r;
   }

   public Event getEvent()
   {
      return event;
   }

   public void setEvent(Event event)
   {
      this.event = event;
   }

   public Member getMember()
   {
      return member;
   }

   public void setMember(Member member)
   {
      this.member = member;
   }

   public String getReason()
   {
      return reason;
   }

   public void setReason(String reason)
   {
      this.reason = reason;
   }
}
