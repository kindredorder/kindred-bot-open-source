package com.kindredorder.discord.data;

import com.kindredorder.discord.db.Access;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by insai on 5/17/2017.
 */
@javax.persistence.Entity
@javax.persistence.Table
@org.eclipse.persistence.annotations.UuidGenerator(name = "uuidGenerator")
@javax.persistence.NamedQueries(
{
      @javax.persistence.NamedQuery(name = "com.kindredorder.discord.data.Frenzy.all",
            query = "select s from Frenzy s"),
      @javax.persistence.NamedQuery(name = "com.kindredorder.discord.data.Frenzy.byId",
            query = "select s from Frenzy s where s.id = :id")
})
public class Frenzy
{
   @javax.persistence.Transient
   private static Logger log = LoggerFactory.getLogger(Frenzy.class.getName());

   @javax.persistence.Id
   @javax.persistence.GeneratedValue(generator = "uuidGenerator")
   @javax.persistence.Column(name = "ID")
   private String id;

   @javax.persistence.Column(name = "SUBMITTER")
   private String submitter;

   @javax.persistence.Column(name = "FRENZY")
   private String frenzy;

   @javax.persistence.Column(name = "DATE")
   private Long date;

   @javax.persistence.Transient
   private float frenzyCount = 1;

   @javax.persistence.Transient
   private String requester = "";

   private Frenzy exists(final javax.persistence.EntityManager em)
   {
      Frenzy ret = null;
      if (em != null)
      {
         synchronized (log)
         {
            try
            {
               final javax.persistence.TypedQuery<Frenzy> q =
                     em.createNamedQuery(Frenzy.class.getCanonicalName() + ".byId", Frenzy.class);
               q.setParameter("id", id);
               ret = q.getSingleResult();
            }
            catch (final javax.persistence.NoResultException nre)
            {
               log.debug("no result - could be expected {}", nre);
               ret = null;
            }
            catch (final Throwable t)
            {
               log.error("", t);
               ret = null;
            }
         }
      }
      return ret;
   }

   public void store()
   {
      synchronized (log)
      {
         final javax.persistence.EntityManager em = Access.instance().entityManager();
         em.getTransaction().begin();
         final Frenzy existing = exists(em);
         if (existing == null)
         {
            em.persist(this);
         }
         else
         {
            em.merge(this);
         }
         em.getTransaction().commit();
         Access.closeEntityManager(em);
      }
   }

   public void remove()
   {
      synchronized (log)
      {
         final javax.persistence.EntityManager em = Access.instance().entityManager();
         em.getTransaction().begin();
         final Frenzy existing = exists(em);
         if (existing != null)
         {
            em.remove(existing);
         }
         em.getTransaction().commit();
         Access.closeEntityManager(em);
      }
   }

   public static Frenzy one(final String id)
   {
      Frenzy ret = null;
      synchronized (log)
      {
         final javax.persistence.EntityManager em = Access.instance().entityManager();
         try
         {
            final javax.persistence.TypedQuery<Frenzy> q =
                  em.createNamedQuery(Frenzy.class.getCanonicalName() + ".byId", Frenzy.class);
            q.setParameter("id", id);
            em.getTransaction().begin();
            ret = q.getSingleResult();
            em.getTransaction().commit();
         }
         catch (final javax.persistence.NoResultException nre)
         {
            log.debug("no result - could be expected {}", nre);
            ret = null;
         }
         catch (final Throwable t)
         {
            log.error("", t);
         }
         finally
         {
            if (em.getTransaction().isActive())
            {
               em.getTransaction().rollback();
            }
            Access.closeEntityManager(em);
         }
      }
      return ret;
   }

   public static java.util.List<Frenzy> all()
   {
      java.util.List<Frenzy> ret = null;
      synchronized (log)
      {
         javax.persistence.EntityManager em = null;
         try
         {
            em = Access.instance().entityManager();
            final javax.persistence.TypedQuery<Frenzy> q = em.createNamedQuery(Frenzy.class.getCanonicalName() + ".all", Frenzy.class);
            em.getTransaction().begin();
            ret = q.getResultList();
            em.getTransaction().commit();
         }
         catch (final Throwable t)
         {
            log.error("", t);
            if (em != null && em.getTransaction().isActive() )
            {
               em.getTransaction().rollback();
            }
         }
         finally
         {
            Access.closeEntityManager(em);
         }
      }
      return ret;
   }

   public static Frenzy getRandomFrenzy()
   {
      final java.util.List<Frenzy> all = Frenzy.all();
      if (all != null && all.size() > 0)
      {
         int rand = ThreadLocalRandom.current().nextInt(0, all.size());
         final Frenzy ret = all.get(rand);
         ret.frenzyCount = all.size();
         return ret;
      }
      return null;
   }

   @Override
   public String toString()
   {
      final StringBuilder sb = new StringBuilder();
      final Date submitted = new Date(date);
      final SimpleDateFormat format = new SimpleDateFormat("MM/dd/YYYY");
      sb.append(requester).append(" \"").append(frenzy).append("\" - ").append(submitter).append(" ").append(format.format(submitted));
      //      .append(" Selected from ").append(Math.round(frenzyCount)).append(" frenzy messages (").append(1/frenzyCount*100).append("% chance)")
      log.info("Frenzy ID: " + id);
      return sb.toString();
   }

   public static Frenzy fromEvent(final MessageReceivedEvent event)
   {
      final String message = event.getMessage().getContent().trim();
      final String[] parts = message.split("\\s+", 2);
      if (parts.length > 1)
      {
         final Date now = new Date();
         final Frenzy newFrenzy = new Frenzy();
         newFrenzy.submitter = event.getMessage().getAuthor().getName();
         newFrenzy.frenzy = parts[1];
         newFrenzy.date = now.getTime();
         return newFrenzy;
      }
      return null;
   }

   public static String idFromEvent(MessageReceivedEvent event)
   {
      final String message = event.getMessage().getContent().trim();
      final String[] parts = message.split("\\s+", 2);
      if (parts.length > 1)
      {
         return parts[1];
      }
      return null;
   }

   public void requester(final String name)
   {
       requester = name;
   }
}
