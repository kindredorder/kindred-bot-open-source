package com.kindredorder.discord.data;

import com.kindredorder.discord.db.Access;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by insai on 5/21/2017.
 */
@javax.persistence.Entity
@javax.persistence.Table
@javax.persistence.NamedQueries(
      {
            @javax.persistence.NamedQuery(name = "com.kindredorder.discord.data.Event.all",
                  query = "select s from Event s order by s.starts asc"),
            @javax.persistence.NamedQuery(name = "com.kindredorder.discord.data.Event.byId",
                  query = "select s from Event s where s.id = :id"),
            @javax.persistence.NamedQuery(name = "com.kindredorder.discord.data.Event.futureEvents",
                  query = "select s from Event s where s.starts > :starts or s.ends > :ends order by s.starts asc")
      })
public class Event
{
   @Transient
   private static Logger log = LoggerFactory.getLogger(Event.class.getName());

   @Id
   @Column
   private String id;

   @Column
   private String name;

   @Column
   private String description;

   @Column
   private Long starts;

   @Column
   private Long ends;

   @ManyToMany
   @JoinTable(name="event_members",
              joinColumns = @JoinColumn(name="event_id", referencedColumnName = "id"),
              inverseJoinColumns = @JoinColumn(name="member_id", referencedColumnName = "discordid"))
   private java.util.List<Member> attending;

   @OneToMany(cascade = CascadeType.ALL, mappedBy = "event")
   private java.util.List<Missing> notAttending;

   public Event()
   {
      super();
   }

   public Event(List<String> parts) throws ParseException
   {
      this();
      id = UUID.randomUUID().toString();
      fromParts(parts);
   }

   public List<Member> getAttending()
   {
      return attending;
   }

   public void setAttending(List<Member> attending)
   {
      this.attending = attending;
   }

   public List<Missing> getNotAttending()
   {
      return notAttending;
   }

   public void setNotAttending(List<Missing> notAttending)
   {
      this.notAttending = notAttending;
   }

   public void addAttending(final Member m)
   {
      if (notAttending != null && notAttending.size() > 0)
      {
         notAttending.removeIf((final Missing miss) -> miss.getMember().equals(m));
      }
      if (attending == null)
      {
         attending = new java.util.ArrayList<>();
      }
      attending.removeIf((final Member member) -> m.equals(member));
      attending.add(m);
   }

   public void addNotAttending(final Member m, final String reason)
   {
      if (attending != null && attending.size() > 0)
      {
         attending.removeIf((final Member member) -> member.equals(m));
      }
      if (notAttending == null)
      {
         notAttending = new java.util.ArrayList<>();
      }
      final Missing miss = new Missing(this, m, reason);
      notAttending.removeIf((final Missing missing) -> missing.getMember().equals(miss.getMember()));
      notAttending.add(miss);
   }

   private Event exists(final javax.persistence.EntityManager em)
   {
      Event ret = null;
      if (em != null)
      {
         synchronized (log)
         {
            try
            {
               final javax.persistence.TypedQuery<Event> q =
                     em.createNamedQuery(Event.class.getCanonicalName() + ".byId", Event.class);
               q.setParameter("id", id);
               ret = q.getSingleResult();
            }
            catch (final javax.persistence.NoResultException nre)
            {
               log.debug("no result - could be expected {}", nre);
               ret = null;
            }
            catch (final Throwable t)
            {
               log.error("", t);
               ret = null;
            }
         }
      }
      return ret;
   }

   public void store()
   {
      synchronized (log)
      {
         final javax.persistence.EntityManager em = Access.instance().entityManager();
         em.getTransaction().begin();
         final Event existing = exists(em);
         if (existing == null)
         {
            em.persist(this);
         }
         else
         {
            em.merge(this);
         }
         em.getTransaction().commit();
         Access.closeEntityManager(em);
      }
   }

   public void remove()
   {
      synchronized (log)
      {
         final javax.persistence.EntityManager em = Access.instance().entityManager();
         em.getTransaction().begin();
         final Event existing = exists(em);
         if (existing != null)
         {
            em.remove(existing);
         }
         em.getTransaction().commit();
         Access.closeEntityManager(em);
      }
   }

   public static Event byId(final String id)
   {
      return one(id);
   }

   private static Event one(final String id)
   {
      Event ret = null;
      synchronized (log)
      {
         final javax.persistence.EntityManager em = Access.instance().entityManager();
         try
         {
            final javax.persistence.TypedQuery<Event> q =
                  em.createNamedQuery(Event.class.getCanonicalName() + ".byId", Event.class);
            q.setParameter("id", id);
            em.getTransaction().begin();
            ret = q.getSingleResult();
            em.getTransaction().commit();
         }
         catch (final javax.persistence.NoResultException nre)
         {
            log.debug("no result - could be expected {}", nre);
            ret = null;
         }
         catch (final Throwable t)
         {
            log.error("", t);
         }
         finally
         {
            if (em.getTransaction().isActive())
            {
               em.getTransaction().rollback();
            }
            Access.closeEntityManager(em);
         }
      }
      return ret;
   }

   public static java.util.List<Event> all()
   {
      java.util.List<Event> ret = null;
      synchronized (log)
      {
         javax.persistence.EntityManager em = null;
         try
         {
            em = Access.instance().entityManager();
            final javax.persistence.TypedQuery<Event> q = em.createNamedQuery(Event.class.getCanonicalName() + ".all", Event.class);
            em.getTransaction().begin();
            ret = q.getResultList();
            em.getTransaction().commit();
         }
         catch (final Throwable t)
         {
            log.error("", t);
            if (em != null && em.getTransaction().isActive() )
            {
               em.getTransaction().rollback();
            }
         }
         finally
         {
            Access.closeEntityManager(em);
         }
      }
      return ret;
   }

   public static java.util.List<Event> futureEvents()
   {
      java.util.List<Event> ret = null;
      synchronized (log)
      {
         javax.persistence.EntityManager em = null;
         try
         {
            Date now = new Date();
            em = Access.instance().entityManager();
            final javax.persistence.TypedQuery<Event> q = em.createNamedQuery(Event.class.getCanonicalName() + ".futureEvents", Event.class);
            q.setParameter("starts", now.getTime());
            q.setParameter("ends", now.getTime());
            em.getTransaction().begin();
            ret = q.getResultList();
            em.getTransaction().commit();
         }
         catch (final Throwable t)
         {
            log.error("", t);
            if (em != null && em.getTransaction().isActive() )
            {
               em.getTransaction().rollback();
            }
         }
         finally
         {
            Access.closeEntityManager(em);
         }
      }
      return ret;
   }

   public String getName()
   {
      return name;
   }

   public String getDescription()
   {
      return description;
   }

   public String getId()
   {
      return id;
   }

   public long getStartTime()
   {
      return starts;
   }

   public long getEndTime()
   {
      return ends;
   }

   public void fromParts(List<String> parts) throws ParseException
   {
      name = parts.get(0);
      description = parts.get(1);
      DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy-hh:mma-VV");
      ZonedDateTime startdate = ZonedDateTime.parse(parts.get(2), format);
      starts = startdate.toInstant().toEpochMilli();
      ZonedDateTime enddate = ZonedDateTime.parse(parts.get(3), format);
      ends = enddate.toInstant().toEpochMilli();
   }
}
