package com.kindredorder.discord.data;

import com.kindredorder.discord.db.Access;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.CascadeType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Set;

/**
 * Created by insai on 5/17/2017.
 */
@javax.persistence.Entity
@javax.persistence.Table
@org.eclipse.persistence.annotations.UuidGenerator(name = "uuidGenerator")
@javax.persistence.NamedQueries(
      {
            @javax.persistence.NamedQuery(name = "com.kindredorder.discord.data.Member.all",
                  query = "select s from Member s"),
            @javax.persistence.NamedQuery(name = "com.kindredorder.discord.data.Member.byId",
                  query = "select s from Member s where s.id = :id"),
            @javax.persistence.NamedQuery(name = "com.kindredorder.discord.data.Member.byDiscordId",
                  query = "select s from Member s where s.discordId = :id")
      })
public class Member
{
   @javax.persistence.Transient
   private static Logger log = LoggerFactory.getLogger(Member.class.getName());

   //@javax.persistence.Id
   @javax.persistence.GeneratedValue(generator = "uuidGenerator")
   @javax.persistence.Column
   private String id;

   @javax.persistence.Id
   @javax.persistence.Column
   private String discordId;

   @javax.persistence.Column
   private String timezone;

   @javax.persistence.Column
   private String characterName;

   @ManyToMany(mappedBy = "attending")
   private Set<Event> attendingEvents;

   @OneToMany(cascade = CascadeType.ALL, mappedBy = "member")
   private Set<Missing> eventsNotAttending;

   private Member()
   {
      super();
   }

   public Member(String discordId, String timezone, String characterName)
   {
      this();
      this.discordId = discordId;
      this.timezone = timezone;
      this.characterName = characterName;
   }

   public static Logger getLog()
   {
      return log;
   }

   public static void setLog(Logger log)
   {
      Member.log = log;
   }

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getDiscordId()
   {
      return discordId;
   }

   public void setDiscordId(String discordId)
   {
      this.discordId = discordId;
   }

   public String getTimezone()
   {
      return timezone;
   }

   public void setTimezone(String timezone)
   {
      this.timezone = timezone;
   }

   public String getCharacterName()
   {
      return characterName;
   }

   public void setCharacterName(String characterName)
   {
      this.characterName = characterName;
   }

   public Set<Event> getAttendingEvents()
   {
      return attendingEvents;
   }

   public void setAttendingEvents(Set<Event> attendingEvents)
   {
      this.attendingEvents = attendingEvents;
   }

   public Set<Missing> getEventsNotAttending()
   {
      return eventsNotAttending;
   }

   public void setEventsNotAttending(Set<Missing> eventsNotAttending)
   {
      this.eventsNotAttending = eventsNotAttending;
   }

   private Member exists(final javax.persistence.EntityManager em)
   {
      Member ret = null;
      if (em != null)
      {
         synchronized (log)
         {
            try
            {
               final javax.persistence.TypedQuery<Member> q =
                     em.createNamedQuery(Member.class.getCanonicalName() + ".byId", Member.class);
               q.setParameter("id", id);
               ret = q.getSingleResult();
            }
            catch (final javax.persistence.NoResultException nre)
            {
               try
               {
                  final javax.persistence.TypedQuery<Member> q =
                        em.createNamedQuery(Member.class.getCanonicalName() + ".byDiscordId", Member.class);
                  q.setParameter("id", discordId);
                  ret = q.getSingleResult();
               }
               catch (final javax.persistence.NoResultException nre2)
               {
                  log.debug("no result - could be expected {}", nre2);
                  ret = null;
               }
               catch (final Throwable t)
               {
                  log.error("", t);
                  ret = null;
               }
            }
            catch (final Throwable t)
            {
               log.error("", t);
               ret = null;
            }
         }
      }
      return ret;
   }

   public void store()
   {
      synchronized (log)
      {
         final javax.persistence.EntityManager em = Access.instance().entityManager();
         em.getTransaction().begin();
         final Member existing = exists(em);
         if (existing == null)
         {
            em.persist(this);
         }
         else
         {
            em.merge(this);
         }
         em.getTransaction().commit();
         Access.closeEntityManager(em);
      }
   }

   public void remove()
   {
      synchronized (log)
      {
         final javax.persistence.EntityManager em = Access.instance().entityManager();
         em.getTransaction().begin();
         final Member existing = exists(em);
         if (existing != null)
         {
            em.remove(existing);
         }
         em.getTransaction().commit();
         Access.closeEntityManager(em);
      }
   }

   public static Member byId(final String id)
   {
      return one(id, ".byId");
   }

   public static Member byDiscordId(final String id)
   {
      return one(id, ".byDiscordId");
   }

   private static Member one(final String id, final String query)
   {
      Member ret = null;
      synchronized (log)
      {
         final javax.persistence.EntityManager em = Access.instance().entityManager();
         try
         {
            final javax.persistence.TypedQuery<Member> q =
                  em.createNamedQuery(Member.class.getCanonicalName() + query, Member.class);
            q.setParameter("id", id);
            em.getTransaction().begin();
            ret = q.getSingleResult();
            em.getTransaction().commit();
         }
         catch (final javax.persistence.NoResultException nre)
         {
            log.debug("no result - could be expected {}", nre);
            ret = null;
         }
         catch (final Throwable t)
         {
            log.error("", t);
         }
         finally
         {
            if (em.getTransaction().isActive())
            {
               em.getTransaction().rollback();
            }
            Access.closeEntityManager(em);
         }
      }
      return ret;
   }

   public static java.util.List<Member> all()
   {
      java.util.List<Member> ret = null;
      synchronized (log)
      {
         javax.persistence.EntityManager em = null;
         try
         {
            em = Access.instance().entityManager();
            final javax.persistence.TypedQuery<Member> q = em.createNamedQuery(Member.class.getCanonicalName() + ".all", Member.class);
            em.getTransaction().begin();
            ret = q.getResultList();
            em.getTransaction().commit();
         }
         catch (final Throwable t)
         {
            log.error("", t);
            if (em != null && em.getTransaction().isActive() )
            {
               em.getTransaction().rollback();
            }
         }
         finally
         {
            Access.closeEntityManager(em);
         }
      }
      return ret;
   }

   @Override
   public boolean equals(Object o)
   {
      boolean ret = true;
      if (o == null)
      {
         ret = false;
      }
      if (!Member.class.isAssignableFrom(o.getClass()))
      {
         ret = false;
      }
      final Member other = (Member) o;
      if (other.discordId == null || !other.discordId.equals(discordId))
      {
         ret = false;
      }
      return ret;
   }
}
