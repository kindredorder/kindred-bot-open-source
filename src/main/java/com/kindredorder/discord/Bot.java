package com.kindredorder.discord;

import com.github.alphahelix00.discordinator.d4j.DiscordinatorModule;
import com.kindredorder.discord.commands.Commands;
import com.kindredorder.discord.db.Access;
import com.kindredorder.discord.events.EventManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.util.DiscordException;

/**
 * Created by insai on 4/30/2017.
 */
public class Bot
{
   private static Logger log = LoggerFactory.getLogger(Bot.class.getName());
   private static EventManager em;

   public static IDiscordClient createClient(final String token, final boolean login)
   {
      IDiscordClient ret = null;
      ClientBuilder clientBuilder = new ClientBuilder();
      clientBuilder.withToken(token);
      try
      {
         if (login) {
            ret = clientBuilder.login();
         }
         else
         {
            ret = clientBuilder.build();
         }
      }
      catch (final DiscordException e)
      {
         e.printStackTrace();
      }
      if (ret != null)
      {
         ret.getModuleLoader().loadModule(new DiscordinatorModule());
      }
      return ret;
   }

   public static void main(final String[] args)
   {
      String token = null;
      em = EventManager.instance();
      if (args.length == 0)
      {
         // TODO - replace the line below with your bot token from discord
         token = "if you don't want to pass your bot token on the command line put it here";
      }
      else
      {
         token = args[0];
      }
      IDiscordClient client = createClient(token, true);
      if (client != null)
      {
         // TODO - Use your own db parameters
         final Access db = Access.instance("0.0.0.0",
                                           1337,
                                           "username",
                                           "password",
                                           "database",
                                           "persistence-unit-name"); // NOTE THIS MUST MATCH THE PERSISTENCE UNIT NAME IN src/main/resources/META-INF/persistence.xml
         db.startServer();
         em.registerEvents(client.getDispatcher());
         Commands.registerCommands(db);
      }
      else
      {
         log.error("Failed to create and login Bot. Exiting.");
         System.exit(1);
      }
   }
}
