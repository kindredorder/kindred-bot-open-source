package com.kindredorder.discord.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.DisconnectedEvent;

/**
 * Created by insai on 4/30/2017.
 */
public class LogoutEvent
{
   private static Logger log = LoggerFactory.getLogger(LogoutEvent.class.getName());

   @EventSubscriber
   public void logout(final DisconnectedEvent event) { // This is called when dDisconnectedEvent is dispatched
      log.info("Logged out for reason " + event.getReason() + "!");
   }
}
