package com.kindredorder.discord.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.Image;
import sx.blah.discord.util.RateLimitException;

import java.io.File;

/**
 * Created by insai on 4/30/2017.
 */
public class ReadyEvent
{
   private static Logger log = LoggerFactory.getLogger(ReadyEvent.class.getName());

   @EventSubscriber
   public void onReadyEvent(final sx.blah.discord.handle.impl.events.ReadyEvent e)
   {
      log.info("ReadyEvent event recieved. " + e.toString());
      final IDiscordClient client = e.getClient();
      try
      {
         // Set the username of your bot
         client.changeUsername("Your User Name Here");
      }
      catch (final DiscordException e1)
      {
         log.error(e1.getErrorMessage());
      }
      catch (final RateLimitException e1)
      {
         log.error(e1.getMessage());
      }
      final IUser ourUser = client.getOurUser();
      log.info("Logged in as " + ourUser.getName());
   }
}
