package com.kindredorder.discord.events;

import sx.blah.discord.api.events.EventDispatcher;

/**
 * Created by insai on 4/30/2017.
 */
public class EventManager
{
   private static EventManager instance;

   private EventManager()
   {
      super();
   }

   public static EventManager instance()
   {
      if (instance == null)
      {
         instance = new EventManager();
      }
      return instance;
   }

   public static void registerEvents(EventDispatcher dispatcher)
   {
      dispatcher.registerListener(new ReadyEvent());
      dispatcher.registerListener(new LogoutEvent());
   }
}
